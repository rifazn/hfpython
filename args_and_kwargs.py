"""Demonstration/example of how to use *args and **kwargs"""

def myfunc(*args):
    for a in args:
        print(a, end=' ')
    if args:
        print()

print('Demoing some use examples of *args...\n')

print('myfunc(1, 2, 3, 4, 5)')
myfunc(1, 2, 3, 4, 5)

print('values=[1,2,3,4,5]', 'myfunc(values)', sep='\n')
values = [1, 2, 3, 4, 5]
myfunc(values)

def myfunc2(**kwargs):
    for k, v in kwargs.items():
        print(k, v, sep='->', end=' ')
    if kwargs:
        print()

print('**kwargs...\n')
print('myfunc()')
myfunc2()
print('myfunc2(a=2, b=3, c=4)')
myfunc2(a=2, b=3, c=4)
print('d={"a":2,"b":3,"c":4}', 'myfunc2(**d)', sep='\n')
d = {"a" : 2, "b" :3, "c" : 4}
myfunc2(**d)

def myfunc3(*args, **kwargs):
    for a in args:
        print(a, end=' ')
    if args:
        print()

    for k, v in kwargs.items():
        print(k, v, sep='->', end=' ')
    if kwargs:
        print()

print('*args, **kwargs')
print('works with a list... myfunc3(1, 2, 3)')
myfunc3(1,2,3)
print('with a dict... myfunc3(a=1, b=2, c=3)')
myfunc3(a=1, b=2, c=3)
print('and also works with a mix of list and dict')
print('myfunc3(1, 2, 3, a=4, b=5, c=6)')
myfunc3(1, 2, 3, a=4, b=5, c=6)
